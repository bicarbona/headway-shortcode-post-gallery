=== Headway Shortcode Post Gallery ===
Contributors:      10up
Donate link:       http://wordpress.org/plugins
Tags: 
Requires at least: 3.5.1
Tested up to:      3.5.1
Stable tag:        0.1.0
License:           GPLv2 or later
License URI:       http://www.gnu.org/licenses/gpl-2.0.html

Custom shortcode post galllery for Miguela

== Description ==



== Installation ==

= Manual Installation =

1. Upload the entire `/headway-shortcode-post-gallery` directory to the `/wp-content/plugins/` directory.
2. Activate Headway Shortcode Post Gallery through the 'Plugins' menu in WordPress.

== Frequently Asked Questions ==


== Screenshots ==


== Changelog ==

= 0.1.0 =
* First release

== Upgrade Notice ==

= 0.1.0 =
First Release
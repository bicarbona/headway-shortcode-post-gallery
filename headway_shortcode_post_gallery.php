<?php
/**
 * Plugin Name: Headway Shortcode Post Gallery
 * Plugin URI:  http://wordpress.org/plugins
 * Description: Custom shortcode post galllery for Miguela
 * Version:     0.1.0
 * Author:      
 * Author URI:  
 * License:     GPLv2+
 * Text Domain: headway_shortcode_post_gallery
 * Domain Path: /languages
 * Bitbucket Plugin URI: https://bitbucket.org/bicarbona/headway-shortcode-post-gallery
 * Bitbucket Branch: master
 */


/**
ADMIN STYLE
**/

add_action( 'admin_enqueue_scripts', 'safely_add_stylesheet_to_admin' );

/**
 * Add stylesheet to the page
 */
function safely_add_stylesheet_to_admin() {
    wp_enqueue_style( 'prefix-style', plugins_url('/admin/admin-style.css', __FILE__) );
}


/*  SHORTCODE */
 
add_shortcode('galeria', 'etienne_gallery_sc');
 
function etienne_gallery_sc( $atts, $content ){
 
  extract( shortcode_atts( array(
		'post_id' => get_the_id(),
		'cislo' => '1',
	), $atts) );
 
  $etienne_gallery = get_field( 'etienne_gallery', $post_id );

	//fix cisla galerie
	$cislo = $cislo -1;
	//echo $cislo;

// Repeater

$rows = get_field('custom_gallerys' ); // get all the rows
$first_row = $rows[$cislo]; // get the first row

//$images = get_field('gallery');

$images = $first_row['custom_gallery' ]; // get the sub field value 

if( $images ): 
	foreach( $images as $image ): 
		
		if ($image['alt']) {
			$alt = 'alt="'.$image['alt'].'"';
		}  
		
		$output .='<li>';
			$output .= '<img src="'.$image['sizes']['thumbnail'].'" '.$alt.' />';
		$output .= '</li>';
	endforeach;
endif; 

return $output;
}
 /**
END SHORTCODE
 **/

/** 

HEADWAY 

**/
/**
 * Everything is ran at the after_setup_theme action to insure that all of Headway's classes and functions are loaded.
 **/
add_action('after_setup_theme', 'headway_shortcode_post_gallery_register');
function headway_shortcode_post_gallery_register() {

	if ( !class_exists('Headway') )
		return;

	require_once 'includes/Block.php';
	require_once 'includes/BlockOptions.php';
	//require_once 'design-editor-settings.php';

	return headway_register_block('headway_shortcode_post_galleryBlock', plugins_url(false, __FILE__));
}

/**
 * Prevent 404ing from breaking Infinite Scrolling
 **/
add_action('status_header', 'headway_shortcode_post_gallery_prevent_404');
function headway_shortcode_post_gallery_prevent_404($status) {

	if ( strpos($status, '404') && get_query_var('paged') && headway_get('pb') )
		return 'HTTP/1.1 200 OK';

	return $status;
}

/**
 * Prevent WordPress redirect from messing up featured board pagination
 */
add_filter('redirect_canonical', 'headway_shortcode_post_gallery_redirect');
function headway_shortcode_post_gallery_redirect($redirect_url) {

	if ( headway_get('pb') )
		return false;

	return $redirect_url;
}
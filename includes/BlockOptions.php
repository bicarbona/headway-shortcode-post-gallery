<?php

    class headway_shortcode_post_galleryBlockOptions extends HeadwayBlockOptionsAPI {

        public $tabs = array(
            'my-only-tab' => 'Settings', 
            'custom-icons' => 'Custom Icons'
			
        );

        public $inputs = array(


            'my-only-tab' => array(

				'first-heading'	=> array(
					'name'	 => 'first-heading',
					'type'	 => 'heading',
					'label'	 => 'Permalinks'
				),
                'image' => array(
                    'type' => 'image',
                    'name' => 'logo',
                    'label' => 'Logo',
                    'tooltip' => 'Put anything'
                ),
					
				'image-position' => array(
					'type' => 'select',
					'name' => 'image-position',
					'label' => 'Image Position',
					'default' => 'left',
					'options' => array(
						'left' => 'Left',
						'right' => 'Right',
						'below-title' => 'Below Title'
					),
					'tooltip' => 'Put anything'
				),
				
				'feature-link-placement' => array(
					'type' => 'multi-select',
					'name' => 'feature-link-placement',
					'label' => 'Link Placement',
					'default' => '0',
					'tooltip' => 'Put anything',
					'options' => array(
						'0' => 'Title',
						'1' => 'Image',
						'2' => 'Readon link'
					)
				),
				
				'show-twitter-button' => array(
					'type' => 'checkbox',
					'name' => 'show-twitter-button', 
					'label' => 'Twitter: Show Tweet Button',
					'default' => false,
					'tooltip' => 'Put anything',
				),
				'columns' => array(
					'type' => 'slider',
					'name' => 'columns', 
					'label' => 'Columns',
					'slider-min' => 1,
					'slider-max' => 7,
					'slider-interval' => 1,
					'default' => 3,
					'tooltip' => 'Put anything'
				),
				
				
				'block-title-type' => array(
					'type' => 'select',
					'name' => 'block-title-type',
					'label' => 'Block Title Type',
					'default' => 'h1',
					'options' => array(
						'h1' => 'h1',
						'h2' => 'h2',
						'h3' => 'h3',
						'h4' => 'h4',
						'h5' => 'h5'
					),
					'tooltip' => 'Put anything'
				),
				
				'pins-per-page' => array(
					'type' => 'integer',
					'name' => 'pins-per-page',
					'label' => 'Pins Per Page',
					'tooltip' => '',
					'default' => 10,
					'tooltip' => 'Put anything <em>infinite</em>.'
				),
				
				'twitter-username' => array(
					'type' => 'text',
					'name' => 'twitter-username', 
					'label' => 'Twitter: Your Username'
				),
				
				'ep-permalink-rel' => array(
					'type'		 => 'textarea',
					'label'		 => 'Permalink rel',
					'tooltip'	 => 'Put anything',
					'default'	 => 'bookmark',
					'name'		 => 'permalink-rel'
				),
				
				'stack-or-float' => array(
					'type' => 'select',
					'name' => 'stack-or-float',
					'label' => 'TOGGLE',
					'tooltip' => '',
					'options' => array(
						'hide-options' => 'Hide',
						'show-options' => 'Show'
					), 
					'default' => 'float',
					'toggle'    => array(
						'hide-options' => array(
							'hide' => array(
								'#input-toggle-header',
								'#input-toggle-slider'
							),
						),
						'show-options' => array(
							'show' => array(
								'#input-toggle-slider',
								'#input-toggle-header'
							),
						)
					),
					'tooltip' => 'Display articles on top of each other (stack) or side by side as a grid (float)',
					'callback' => 'id = $(input).attr("block_id");'
				),
				
				'toggle-header' => array(
					'type' => 'heading',
					'name' => 'toggle-header',
					'label' => 'Toggle Heading',
				),

				'toggle-slider' => array(
					'type' => 'slider',
					'name' => 'toggle-slider', 
					'label' => 'Columns (iPhone/Smartphone)',
					'slider-min' => 1,
					'slider-max' => 8,
					'slider-interval' => 1,
					'default' => 2,
					'tooltip' => 'NB! Headway needs to be set to responsive in the grid first. Set how many articles to display horizontally for iPhones and smartphones.  <strong>Recommended setting: 1 or 2</strong>'
				),
					
            ),
				

			'custom-icons' => array(
				'icons' => array(
					'type' => 'repeater',
					'name' => 'icons',
					'label' => 'Icons',
					'inputs' => array(
						// array(
						// 	'type' => 'image',
						// 	'name' => 'image',
						// 	'label' => 'Image',
						// 	'default' => null
						// ),
						
						 array(
							'type' => 'select',
							'name' => 'image',
							'label' => 'View',
							'default' => 0,
							'tooltip' => 'Set whether the layout single or index view.',
							'options' => array(
								'fa fa-facebook' => 'Facebook',
								//	'fa fa-facebook-square' => '',
								
								'fa fa-twitter' => 'Twitter',
								'fa fa-pinterest' => 'Pinterest',
								'fa fa-pinterest-square' => 'Pinterest square',
								
								'fa fa-android' => 'Android',
								'fa fa-tumblr-square' => 'Tumblr square',
								'fa fa-foursquare' => 'Foursquare',
							
								'fa fa-linkedin' => 'Linked in',
								'fa fa-github' => 'Github',
								'fa fa-github-alt' => 'Github alt',
								'fa fa-weibo' => 'Weibo',
								'fa fa-xing' => 'Xing',
								'fa fa-xing' => 'Xing square',
									'stack-exchange' => 'Stack exchange',
								
								
								'fa fa-github-square' => 'Github square',
								'fa fa-html5' => 'Html 5',
								
								'fa fa-gittip' => 'Gittip',
								'fa fa-bitbucket' => 'Bitbucket',
								'fa fa-css3' => 'CSS 3',
								'fa fa-dribbble' => 'Dribbble',
								'fa fa-trello' => 'Trello',
								'fa fa-renren' => 'Renren',
								'fa fa-linux' => 'Linux',
								'fa fa-stack-overflow' => 'Overflow',
								'fa fa-windows' => 'Windows',
								
								
								'fa fa-maxcdn' => 'Max CDN',
							
									'fa fa-linkedin-square' => 'Linked in square',
									'fa fa-youtube-square' => 'Youtube square',
									'fa fa-youtube-play' => 'Youtube play',
									'fa fa-youtube' => 'Youtube',
									'fa fa-dropbox' => 'Dropbox',
								//	'' => '',
								//	'' => '',
								
									'fa fa-apple' => 'Apple',
								//	'' => '',
								//	'' => '',
								//	'' => '',
								'fa fa-google-plus' => 'Google+',
								'fa fa-google-plus-square' => 'Google+ square',
								'fa fa-instagram' => 'Instagram',
								'fa fa-skype' => 'Skype',
								'fa fa-vimeo-square' => 'Vimeo square',
								'fa fa-tumblr' => '',
								
							)
						),

						array(
							'type' => 'text',
							'name' => 'image-title',
							'label' => '"title"',
							'tooltip' => 'This will be used as the "title" attribute for the image. The title attribute is beneficial for SEO (Search Engine Optimization) and will allow your visitors to move their mouse over the image and read about it.'
						),

						array(
							'type' => 'text',
							'name' => 'image-alt',
							'label' => '"alt"',
							'tooltip' => 'This will be used as the "alt" attribute for the image.  The alt attribute is <em>hugely</em> beneficial for SEO (Search Engine Optimization) and for general accessibility.'
						),

						array(
							'name' => 'link-heading',
							'type' => 'heading',
							'label' => 'Link Image'
						),

						array(
							'name' => 'link-url',
							'label' => 'Link URL?',
							'type' => 'text',
							'tooltip' => 'Set the URL for the image to link to'
						),

						array(
							'name' => 'link-alt',
							'label' => '"alt"',
							'type' => 'text',
							'tooltip' => 'Set alternative text for the link'
						),

						array(
							'name' => 'link-target',
							'label' => 'New window?',
							'type' => 'checkbox',
							'tooltip' => 'If you would like to open the link in a new window check this option',
							'default' => false,
						)

					),
					//'tooltip' => 'Upload the images that you would like to add to the image block.',
					'sortable' => true,
					'limit' => false
				),
			),
				
        );
    }